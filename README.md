# HTTP listener for [Arabesque](https://www.npmjs.com/package/@arabesque/core)

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a merge request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/@arabesque%2Flistener-http.svg

[npm-url]: https://www.npmjs.com/package/@arabesque/listener-http

[coverage-image]: https://coveralls.io/repos/gitlab/arabesque/listener-http/badge.svg

[coverage-url]: https://coveralls.io/gitlab/arabesque/listener-http