import type {Listener} from "@arabesque/core";

export interface ServerResponse {
    end(callback: () => void): void;
}

export interface Server<
    Request extends any = any,
    Response extends ServerResponse = ServerResponse
> {
    close(callback?: (error?: Error) => void): void;

    listen(port?: number, callback?: () => void): void;

    on(eventName: "request", listener: (message: Request, response: Response) => void): void;
}

export type Context<Message extends any = any, Response extends ServerResponse = ServerResponse> = {
    message: Message;
    response: Response;
};

export const createListener = <Message extends any, Response extends ServerResponse>(
    server: Server<Message, Response>
): Listener<number, Context<Message, Response>> => {
    return (channel, handle) => {
        return new Promise((resolve) => {
            server.on("request", (message: Message, response: Response) => {
                const context = {message, response};

                return handle(context).then((context) => {
                    return new Promise((resolve) => {
                        const {response} = context;

                        response.end(() => resolve(undefined));
                    });
                });
            });

            server.listen(channel, () => {
                const stop = (): Promise<void> => {
                    return new Promise((resolve) => {
                        server.close(() => {
                            resolve();
                        })
                    });
                };

                resolve(stop);
            });
        });
    };
}