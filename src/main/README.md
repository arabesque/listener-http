# HTTP listener for [Arabesque](https://www.npmjs.com/package/@arabesque/core)

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

HTTP listener for Arabesque consists of a context type and a factory that returns an Arabesque Listener.

## Usage

### HTTP

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer} from "http";

const listener = createListener(createServer());

const application = createApplication(listener);

application.listen(3000);
```

### HTTPS

```ts
import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer} from "https";
import {readFileSync} from "fs";

const listener = createListener(createServer({
    key: readFileSync('key.pem'),
    cert: readFileSync('cert.pem'),
    passphrase: 'passphrase'
}));

const application = createApplication(listener);

application.listen(3000);
```

#### Using a custom response

```ts
import {createApplication} from "@arabesque/core";
import {createListener, ServerResponse} from "@arabesque/listener-http";
import {createServer, IncomingMessage} from "http";

class CustomResponse<Request extends IncomingMessage = IncomingMessage> extends ServerResponse<Request> {
    public body: string | undefined;

    end(cb: () => void) {
        return super.end(this.body, cb);
    }
}

const listener = createListener<IncomingMessage, CustomResponse>(createServer({
    ServerResponse: CustomResponse
}));

const application = createApplication(listener, (context, next) => {
    context.response.body = 'foo';

    return next(context);
});

application(3000);
```