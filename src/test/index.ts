import tape from "tape";
import {IncomingMessage, ServerResponse, RequestListener} from "http";
import {createListener, type Server} from "../main/index";
import {spy, stub} from "sinon";
import {Socket} from "net";

tape('listener', ({same, end}) => {
    let passedOnRequestHandler: RequestListener;

    const client: Server = {
        on: (eventName, handler) => {
            passedOnRequestHandler = handler as RequestListener;
        },
        listen: (port, listeningListener) => {
            (listeningListener as () => void)();
        },
        close: (callback) => {
            callback!();
        }
    };

    const spiedClientClose = spy(client, "close");

    const spiedHandler = spy((context) => {
        return Promise.resolve(context);
    });

    const listener = createListener(client);

    return listener(1234, spiedHandler).then((stop) => {
        const message = new IncomingMessage({} as Socket)
        const response = new ServerResponse(message);

        stub(response, "end").callsFake(((callback: () => void) => {
            callback();
        }) as any);

        // simulate a request received by the client
        passedOnRequestHandler(message, response);

        same(spiedHandler.called, true, 'should call the handler when a message is received');

        return stop().then(() => {
            same(spiedClientClose.called, true, 'should close the client when stopped');
        });
    }).finally(end);
});